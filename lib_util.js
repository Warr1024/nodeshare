function canprobablyread(stat) {
        var euid = process.geteuid();
        if(euid == 0)
                return true;
        var isdir = stat.isDirectory();
        if((stat.mode & 0o004) && (!isdir || (stat.mode & 0o001)))
                return true;
        if((euid == stat.uid) && (stat.mode & 0o400)
                && (!isdir || (stat.mode & 0o100)))
                return true;
        if(process.getgroups().some(x => x == stat.gid)
                && (stat.mode & 0o040)
                && (!isdir || (stat.mode & 0o010)))
                return true;
}

module.exports = {
	canprobablyread: canprobablyread
}
