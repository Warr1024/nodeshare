const minimist = require("minimist");

var opts = minimist(process.argv.slice(2), {default: {
	port: 8090,
	host: "127.0.0.1",
	root: process.env.HOME,
	key: process.env.NODESHARE_KEY,
	thumbzip: ".fcgithumbs.zip",
	mount: "/",
	rssdefault: 25,
	workers: false
}});

var missing = "port host root key".split(" ").filter(x =>
	!opts[x] || !("" + opts[x]).length);
if(missing.length)
	throw("missing required config: " + missing.join(" "));

if(!opts.sharename) {
	var m = opts.mount.match(/[^/].*[^/]/);
	if(m && m[0].length)
		opts.sharename = m[0].toString();
}

if(opts.workers && (typeof(opts.workers) != "number"))
	opts.workers = require("os").cpus().length;

opts.display = JSON.stringify(Object.assign(Object.assign({}, opts),
	{key: opts.key.replace(/./g, "*")}));

module.exports = opts;
