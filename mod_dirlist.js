const fs = require("fs");
const natsort = require("node-natural-sort");
const path = require("path");
const process = require("process");
const pug = require("pug");
const yauzl = require("yauzl");
const lib_config = require("./lib_config");
const lib_ezpug = require("./lib_ezpug");
const lib_util = require("./lib_util");
const mod_shareauth = require("./mod_shareauth");

var template = lib_ezpug(__filename);

function thumbs(lsdir, list, cb) {
	if(!lib_config.thumbzip || !lib_config.thumbzip.length)
		return cb();
	var dict = {};
	for(var i = 0; i < list.length; i++)
		dict[list[i].name] = list[i];
	yauzl.open(path.join(lsdir, lib_config.thumbzip), {lazyEntries: true}, function(err, zip) {
		if(err) return cb()
		zip.on("entry", function(e) {
			if(e.fileName.match(/\/$/))
				return zip.readEntry();
			if(!e.fileName.startsWith("thumb-"))
				return zip.readEntry();
			var k = e.fileName.substr("thumb-".length);
			var ent = dict[k];
			if(!ent)
				return zip.readEntry();
			return zip.openReadStream(e, (err, str) => {
				if(err) return zip.readEntry();
				str.on("data", buff => (ent.thumb = ent.thumb || []).push(buff));
				str.on("end", () => zip.readEntry());
			});
		});
		zip.on("close", cb);
		return zip.readEntry();
	});
}

function entstat(lsdir, ent, cb, req) {
	var fullpath = path.join(lsdir, ent.name);
	fs.stat(fullpath, (err, stat) => {
		if(err) return cb(err);
		if(!lib_util.canprobablyread(stat)) {
			ent.deny = true;
			return cb();
		}
		if(stat.isDirectory()) {
			ent.isdir = true;
		} else {
			var siPref = "\xa0kMGTPEZY";
			var s = stat.size;
			while(s >= 1000) {
				s /= 1000;
				siPref = siPref.substr(1);
			}
			ent.size = s.toFixed(2) + siPref[0];
		}
		ent.uri = ent.name + (ent.isdir ? "/" : "")
			+ "?auth=" + ent.auth
			+ (!req.query.dlall ? "" : ent.isdir ? "&dlall=1" : "&dl=1");
		if(!ent.isdir && (ent.name == "README"))
			return fs.readFile(fullpath, (err, data) => {
				if(err) return cb(err);
				ent.readme = data.toString();
				return cb();
			});
		return cb();
	});
}

function dirdata(lsdir, auth, req, cb) {
	return fs.readdir(lsdir, (err, list) => {
		if(err) 
			return cb(err);

		var errs = [];
		var waits = 0;
		function metacb(err) {
			if(err) errs.push(err);
			if(--waits) return;
			if(errs.length)
				return cb(errs.join(" | "));
			return cb(undefined, list);
		}

		list = list.filter(x => !x.startsWith("."));
		for(var i = 0; i < list.length; i++) {
			list[i] = {
				name: list[i],
				auth: mod_shareauth.derive(auth, list[i])
			};
			waits++;
			entstat(lsdir, list[i], metacb, req);
		}
		if(waits <= 0)
			return cb(undefined, list);
	});
}

function listing(req, res, next) {
	var lsdir = path.join(lib_config.root, req.path);
	var auth = req.query.auth;
	dirdata(lsdir, auth, req, (err, list) => {
		if(err) return res.myfail(err);
		thumbs(lsdir, list, () => {
			var data = {
				title: req.path.replace(/\/+$/, "")
					.replace(/.*\//, ""),
				auth: req.query.auth,
				thumbs: [],
				dirs: [],
				files: []
			};
			if(data.title.length < 1)
				data.title = lib_config.sharename || "/";
			for(var i = 0; i < list.length; i++) {
				if(list[i].deny)
					continue;
				else if(list[i].thumb) {
					list[i].thumb = "data:image/jpeg;base64,"
						+ Buffer.concat(list[i].thumb).toString("base64");
					data.thumbs.push(list[i]);
				} else if(list[i].isdir)
					data.dirs.push(list[i]);
				else if(list[i].readme)
					data.readme = list[i];
				else
					data.files.push(list[i]);
			}
			function byname(a, b) {
				
			}
			var mysort = natsort();
			for(var k in {thumbs: 1, dirs: 1, files: 1}) {
				if(!data[k].length)
					delete data[k];
				else
					data[k].sort((a, b) => mysort(a.name, b.name));
			}
			if(!data.dirs && !data.files && !data.readme && !data.thumbs) {
				res.setHeader("Content-Type", "text/plain");
				return res.send("empty directory").end();
			}
			res.setHeader("Content-Type", "text/html")
			return res.send(template(data)).end();
		});
	});
}

module.exports = listing;
