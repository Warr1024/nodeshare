const isbinaryfile = require("isbinaryfile");
const mime = require("mime");
const path = require("path");
const lib_config = require("./lib_config");

function check(req, res, next) {
	const dl = req.query.dl || req.query.fn;
	const fn = req.query.fn || req.path.replace(/.*\//, "") || "index.html";
	res.setHeader("Content-Disposition", (dl ? "attachment" : "inline")
		+ "; filename=" + JSON.stringify(fn));
	if(req.query.dl || req.query.fn) {
		res.setHeader("Content-Type", "application/octet-stream");
		return next();
	}
	var m = mime.lookup(req.path);
	if(m != "application/octet-stream") {
		res.setHeader("Content-Type", m);
		return next();
	}
	isbinaryfile(path.join(lib_config.root, req.path), (err, bin) => {
		if(err) return res.myfail(err);
		if(!bin)
			res.setHeader("Content-Type", "text/plain");
		return next();
	});
}

module.exports = check;
