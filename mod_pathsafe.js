const fs = require("fs");
const path = require("path");
const lib_config = require("./lib_config");

function pathsafe(root, parts, cb) {
	if(!parts || !parts.length)
		return cb(true);
	if(parts[0].match(/^\./))
		return cb();
	fs.readdir(root, (err, list) => {
		if(err)
			return cb();
		for(var i = 0; i < list.length; i++)
			if(list[i] == parts[0])
				return pathsafe(path.join(root, parts[0]),
					parts.slice(1), cb);
		return cb();
	});
}

function check(req, res, next) {
	var path = req.path.split("/").filter(x => x.length);
	return pathsafe(lib_config.root, path, ok => {
		if(!ok) return res.mydeny("invalid path");
		return next();
	});
};

module.exports = check;
