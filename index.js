const lib_config = require("./lib_config");
const startup = require("./startup");

if(!lib_config.workers) {
	console.log("config: " + lib_config.display);
	startup();
} else {
	const cluster = require("cluster");
	
	var oldlog = console.log;
	console.log = function() {
		if(cluster.isMaster)
			arguments[0] = "[master] " + arguments[0];
		else
			arguments[0] = "[worker " + cluster.worker.id + "] " + arguments[0];
		return oldlog.apply(this, arguments);
	};

	if(cluster.isMaster) {
		console.log("config: " + lib_config.display);
		for(var i = 0; i < lib_config.workers; i++)
			cluster.fork();
		cluster.on("exit", () => cluster.fork());
	} else
		startup();
}
