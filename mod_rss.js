const crypto = require("crypto");
const glob = require("glob");
const fs = require("fs");
const path = require("path");
const lib_config = require("./lib_config");
const lib_ezpug = require("./lib_ezpug");
const mod_shareauth = require("./mod_shareauth");

var template = lib_ezpug(__filename);

function binsert(arr, ent, min, max) {
	if(min >= max)
		return arr.splice(min, 0, ent);
	var mid = Math.floor((min + max) / 2);
	if(ent.mtime > arr[mid].mtime)
		return binsert(arr, ent, min, mid);
	return binsert(arr, ent, mid + 1, max);
}

function handle(req, res, next) {
	if(!req.query.rss)
		return next();
	var dir = path.join(lib_config.root, req.path).replace(/\/+$/, "");
	fs.stat(dir, (err, stat) => {
		if(err) return res.myfail(err);
		if(!stat.isDirectory())
			return res.mydeny("not a directory");

		var name = req.path.replace(/\/+$/, "").replace(/.*\//, "");
		if(!name.length)
			name = lib_config.sharename || "all";

		var absroot = (
			req.headers["x-forwarded-proto"]
			|| req.protocol
		) + "://" + (
			req.headers["x-forwarded-host"]
			|| req.headers.host
			|| (req.hostname + ":" + lib_config.port)
		);

		var ents = [];
		var maxents = Number(req.query.rss) || lib_config.rssdefault;

		var g = glob("**", {
			cwd: dir,
			dot: false,
			nodir: true,
			follow: true
		});
		g.on("stat", (f, s) => {
			s.name = f;
			if(ents.length) {
				binsert(ents, s, 0, ents.length);
				if(ents.length > maxents)
					ents.splice(maxents);
			}
			else
				ents.push(s);
		});
		g.on("end", () => {
			res.setHeader("Content-Type", "application/rss+xml");

			var data = {
				baseUrl: req.baseUrl,
				path: req.path,
				originalUrl: req.originalUrl,
				absroot: absroot,
				name: name,
				items: []
			};
			for(var i = 0; i < ents.length; i++) {
				var s = ents[i];
				s.guid = crypto.createHash("md5")
					.update(s.name + " " + s.mtime.toISOString() + " " + s.size)
					.digest("hex");
				s.auth = mod_shareauth.allauth(req, req.path + s.name).pop();
				data.items.push(s);
			}

			res.send(template(data));
		});
	});
}

module.exports = handle;
