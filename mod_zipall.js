const archiver = require("archiver");
const glob = require("glob");
const fs = require("fs");
const path = require("path");
const lib_config = require("./lib_config");
const lib_util = require("./lib_util");

const formats = {zip: true, tar: true};

function handle(req, res, next) {
	var fmt = req.query.archive;
	if(!fmt || !formats[fmt])
		return next();
	var dir = path.join(lib_config.root, req.path).replace(/\/+$/, "");
	fs.stat(dir, (err, stat) => {
		if(err) return res.myfail(err);
		if(!stat.isDirectory())
			return res.mydeny("not a directory");

		var name = req.path.replace(/\/+$/, "").replace(/.*\//, "");
		if(!name.length)
			name = lib_config.sharename || "all";
		res.attachment(req.query.fn || (name + "." + fmt));

		var archive = archiver(fmt, {store: true});
		archive.on("error", err => res.myfail(err));
		archive.pipe(res);

		var g = glob("**", {
			cwd: dir,
			dot: false,
			nodir: true,
			follow: true
		});
		g.on("stat", (f, s) => {
			if(lib_util.canprobablyread(s))
				archive.file(path.join(dir, f), {name: f});
		});
		g.on("end", () => archive.finalize());
	});
}

module.exports = handle;
