const fs = require("fs");
const pug = require("pug");
const lib_config = require("./lib_config");

function htmlunicode(s) {
	var buff = "";
	for(var i = 0; i < s.length; i++) {
		var c = s.charCodeAt(i);
		if((c >= 127) || ((c < 32) && (c != 9) && (c != 10) && (c != 13)))
			buff += "&#" + c + ";";
		else
			buff += s[i];
	}
	return buff;
}

pug.filters = pug.filters || {};
pug.filters.cssmin = function(str, opts) {
	return str
		.replace(/([,:;{}])\s+/g, m => m[0])
		.replace(/\s+(?=[:;{}])/g, "")
		.replace(/;}/g, "}");
};

module.exports = function(name) {
	var modbase = name.replace(/\.[A-Za-z0-9]+$/, "");
	var temp = pug.compileFile(modbase + ".pug");
	return function(data) {
		data.config = lib_config;
		return htmlunicode(temp(data));
	};
}
