const crypto = require("crypto");
const lib_config = require("./lib_config");

function handle(req, res, next) {
	res.myfail = function(msg) {
		var id = crypto.randomBytes(12).toString("hex");
		console.log("error " + id + ": " + msg);
		try {
			res.status(500);
			res.setHeader("Content-Type", "text/plain");
			res.send("internal error " + id);
		} catch(err) { }
		return res.end();
	};
	res.mydeny = function(msg) {
		res.status(403);
		res.setHeader("Content-Type", "text/plain");
		res.send(msg);
		return res.end();
	};
	res.redirend = function(uri) {
		res.redirect(uri);
		return res.end();
	}

	var sh = res.setHeader;
	res.setHeader = function(n, v) {
		if(n.toLowerCase() == "location") {
			var prel = req.path.replace(/[^\/]+$/, "");
			if(v.startsWith(prel))
				arguments[1] = v.substr(prel.length);
		}
		sh.apply(res, arguments);
	};

	return next();
}

module.exports = handle;
