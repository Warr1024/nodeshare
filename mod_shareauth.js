const crypto = require("crypto");
const lib_config = require("./lib_config");

function derive(key, path) {
	var mac = crypto.createHmac("sha256", key);
	mac.update(path);
	return mac.digest("base64").replace(/[^A-Za-z0-9]+/g, "-").substr(0, 16);
}

function allauth(req, path) {
	var keys = [lib_config.key];
	if(lib_config.sharename)
		keys.push(derive(keys[keys.length - 1], lib_config.sharename));
	keys.push(derive(keys[keys.length - 1], req.hostname));

	if(!path)
		path = req.path;
	if(!Array.isArray(path))
		path = path.split("/").filter(x => x.length);
	for(var i = 0; i < path.length; i++)
		keys.push(derive(keys[keys.length - 1], path[i]));

	return keys;
}

function check(req, res, next) {
	var auth = req.query.auth || "";
	if(!auth)
		return res.mydeny("auth required");

	var valid = allauth(req);
	var authok;
	for(var i = 0; i < valid.length; i++)
		authok = authok || auth == valid[i];

	if(!authok)
		return res.mydeny("invalid auth");

	var canon = valid[valid.length - 1];
	if(auth != canon)
		return res.redirend(req.originalUrl
			.replace(/.*\//, "")
			.replace(/[&?]auth=[^&]*/, m => m[0] + "auth=" + canon));

	return next();
}

check.derive = derive;
check.allauth = allauth;
module.exports = check;
