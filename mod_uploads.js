const busboy = require("busboy");
const fs = require("fs");
const path = require("path");
const lib_config = require("./lib_config");
const lib_ezpug = require("./lib_ezpug");
const mod_shareauth = require("./mod_shareauth");

const UPLOAD = "UPLOAD";

var template = lib_ezpug(__filename);

function pad(s, l) {
	var p = "0";
	while(p.length < l)
		p = p + p;
	s = p + s;
	return s.substr(s.length - l);
}
function stamp() {
	var d = new Date();
	return pad(d.getFullYear(), 4)
		+ pad(d.getMonth() + 1, 2)
		+ pad(d.getDate(), 2)
		+ pad(d.getHours(), 2)
		+ pad(d.getMinutes(), 2)
		+ pad(d.getSeconds(), 2)
		+ pad(d.getMilliseconds(), 3);
}

function newauth(req, parts, base) {
	parts = parts.slice();
	parts.push(base);
	var keys = mod_shareauth.allauth(req, parts);
	return keys[keys.length - 1];
}

function handle(req, res, next) {
	var parts = req.path.split("/").filter(x => x.length);
	if(parts.pop() != UPLOAD)
		return next();
	if(req.method == "POST") {
		var bb = new busboy({headers: req.headers});
		var done;
		bb.on("file", (name, file, filename, encoding, mimetype) => {
			if(done) return;
			var base = stamp() + "-" + filename.replace(/[^A-Za-z0-9\.-]+/g, "-");
			done = { name: base, auth: newauth(req, parts, base) };
			done.uri = done.name + "?auth=" + done.auth;
			file.on("end", () => res.redirend(UPLOAD + "?auth=" + req.query.auth
				+ "&fn=" + base + "&fa=" + done.auth));
			file.pipe(fs.createWriteStream(path.join(lib_config.root, path.join.apply(path, parts), base)));
		});
		bb.on("finish", () => { if(!done) res.redirend(req.url); });
		return req.pipe(bb);
	}
	return fs.readFile(path.join(lib_config.root, req.path), (err, data) => {
		if(err) return res.myfail(err);
		res.setHeader("Content-Type", "text/html");
		var data = {
			title: UPLOAD,
			text: data.toString(),
			uri: UPLOAD + "?auth=" + req.query.auth
		};
		if(req.query.fn && req.query.fa)
			data.done = {
				name: req.query.fn,
				auth: req.query.fa,
				uri: req.query.fn + "?auth=" + req.query.fa
			};
		res.send(template(data)).end();
	});
}

module.exports = handle;
