const fs = require("fs");
const mime = require("mime");
const lib_config = require("./lib_config");

var map = JSON.parse(fs.readFileSync(__dirname + "/mimetypes.json"));
for(var k in map)
	if(map.hasOwnProperty(k))
		if(typeof(map[k]) == "string")
			map[k] = map[k].split(/\s+/);
mime.define(map, true);
	
function startup() {
	const express = require("express");

	var app = express();
	app.use(
		lib_config.mount,
		require("./mod_requtil"),
		require("./mod_shareauth"),
		require("./mod_pathsafe"),
		require("./mod_uploads"),
		require("./mod_rss"),
		require("./mod_zipall"),
		require("./mod_mimetype"),
		express.static(lib_config.root, {index: false}),
		require("./mod_dirlist")
	);

	if(lib_config.tlscert && lib_config.tlskey) {
		var fs = require("fs");
		app = require("https").createServer({
			cert: fs.readFileSync(lib_config.tlscert),
			key: fs.readFileSync(lib_config.tlskey)
		}, app);
	}

	if(lib_config.listen)
		app.listen(lib_config.listen, () =>
			console.log("listening on " + lib_config.listen));
	else
		app.listen(lib_config.port, lib_config.host, () =>
			console.log("listening on " + lib_config.host + ":" + lib_config.port));
}

module.exports = startup;
